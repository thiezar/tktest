<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ServerLoad extends Model
{
    protected $table = 'dati_carico_server';
}
