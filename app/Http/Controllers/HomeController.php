<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('home', [
            'servers' => Auth::user()->servers
        ]);
    }

    /**
     * Calculate the server load on period
     * specified by startDate and endDate request inputs.
     * The server status partial view is embedded in the JSON response.
     */
    public function serverStatus($serverId, Request $request)
    {
        if(!\App\Server::where('id', $serverId)->exists()){
            abort(404); // server does not exist
        }
        $server = Auth::user()->servers->where('id', $serverId)->first();
        if(!$server){
            abort(403); // logged user has no access to the selected server
        }

        $startDate = $request->has('startDate') ? Carbon::parse($request->input('startDate'))->format('Y-m-d') : Carbon::createFromTimestamp(0);
        $endDate = $request->has('endDate') ? Carbon::parse($request->input('endDate'))->format('Y-m-d') : Carbon::createFromTimestamp(PHP_INT_MAX);

        $serverPitches = $server->hourlyAverageLoadPitches($startDate, $endDate, 85);

        // Build the view
        $style = 'success';
        $message = "hourly average load has never been more than 85% between {$startDate} and {$endDate}";
        if($serverPitches > 0){
            $style='warning';
            $message = "hourly average load has reached 85% three times or less between {$startDate} and {$endDate}";
        }
        if($serverPitches > 3){
            $style='danger';
            $message = "hourly average load has reached 85% more than three times between {$startDate} and {$endDate}";
        }

        $view = view('partials.server-status', [
            'style' => $style,
            'message' => $message,
            'server'=>$server
        ])->render();

        return response()->json([
            'html' => $view
        ]);
    }
}
