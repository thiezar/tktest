<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Server extends Model
{
    protected $table = 'server';

    /**
     * The server load
     */
    public function serverLoad()
    {
        return $this->hasMany('App\ServerLoad', 'id_server');
    }

    /**
     * Calculates the hourly average server load on period
     * specified by startDate and endDate.
     */
    public function hourlyAverageServerLoad($startDate, $endDate)
    {
        return \App\ServerLoad::select(\DB::raw("CONCAT(data, \"-\", ora) AS dataora, AVG(carico_medio_cpu_percent) AS media_oraria"))
            ->where('id_server', $this->id)
            ->where('data', '>=', $startDate)
            ->where('data', '<=', $endDate)
            ->groupBy('dataora')->get();
    }

    /**
     * Calculates how many times the hourly average server load on a specifiend period
     * reaches the given load threshold.
     */
    public function hourlyAverageLoadPitches($startDate, $endDate, $loadThreshold)
    {
        return $this->hourlyAverageServerLoad($startDate, $endDate)
                    ->where('media_oraria', '>', $loadThreshold)->count();
    }
}
