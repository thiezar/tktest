const loader = '<div class="spinner-border" role="status"><span class="sr-only">Loading...</span></div>';

const serverSelect = document.getElementById("server-select");
const result = document.getElementById('server-status');

if(serverSelect && result){
    serverSelect.onchange = function(event){
        getSelectedServerStatus();
    }

    function getSelectedServerStatus(){
        result.innerHTML = loader;
        axios.get('/server/'+serverSelect.value+'/status?startDate=2018-08-01&endDate=2018-08-31')
            .then(res => {
                result.innerHTML = res.data.html;
            }).catch(err => {
                result.textContent = err;
            });
    }

    getSelectedServerStatus();
}
