@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-sm-9 col-md-7 col-lg-5 mx-auto">
            <h1>Hello, {{ Auth::user()->username }}</h1>
            <form class="my-4">
                <div class="form-group">
                    <label for="server-select">{{ __('Select a server') }}</label>
                    <select class="form-control" id="server-select">
                        @foreach ($servers as $server)
                            <option value={{ $server->id }}>{{ $server->nome }}</option>
                        @endforeach
                    </select>
                </div>
            </form>
            <div id="server-status"></div>
        </div>
    </div>
</div>
@endsection
