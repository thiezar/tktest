<div class="alert alert-{{ $style }}" role="alert">
    <b>{{ $server->nome }} ({{ $server->ip }})</b> {{ $message }}
</div>
